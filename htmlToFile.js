// this script gets data ajax post request and saves it to a file

//  -- setting up info  -- // 

// for client script

// $('#page-area').click(function () {
//
//     var textVal = $('#textbox').val();
//    
//     $.ajax({
//         type: 'POST',
//         url: '/saveToFile',
//         data: {'text': textVal}
//     });
//
// });



//////////// ***    to put in server file   *** /////////////

// var express = require('express');
// var bodyParser = require('body-parser');
// var app = express();
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));

//////////// ***    to put in server file   *** /////////////




// actual module

//////////// ************** /////////////


var fsPath = require('fs-path');


//////////// ************** /////////////


module.exports.ProcessReqData = function ProcessReqData(req, dir)
{
    var content = req.body.text;
    var css = req.body.css;
    var templateName = req.body.templateName;
    // console.log(req.body.text);
    console.log('dir -- ' + dir);

    // save data to a file
    fsPath.writeFile(dir + templateName + ".html", content , function(err){
        if(err) {
            throw err;
        } else {
            console.log('new template created -- ' + templateName);
        }
    });

    // save css to a file
    fsPath.writeFile(dir + templateName + ".css", css , function(err){
        if(err) {
            throw err;
        } else {
            console.log('css for template created -- ' + templateName);
        }
    });
}