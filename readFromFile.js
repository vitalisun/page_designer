var fs = require("fs");

module.exports.Read = function Read(path)
{
    
    return fs.readFileSync(path, 'utf8');
};

