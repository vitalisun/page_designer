var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
var app = express();
app.use(express.static('public/editor'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// getting modules
var readFromFileScr = require('./readFromFile.js');
var htmlToFileScr = require('./htmlToFile.js');
var checkPathScr = require('./checkPath.js');
var getTemplatesScr = require('./getTemplates.js');

///////////////////////////////////////////////////////////////////////




// getting data from client and saving to a file
app.post('/saveToFile', function (req, res) {

    // 'templateTab':template_tab

    var templTab = req.body.templateTab;
    var actualDir;
    if(templTab === 'basic'){
        actualDir = './public/editor/templates/bootstrapBasic/';
    }else if(templTab === 'more'){
        actualDir = './public/editor/templates/components/';
    }else if(templTab === 'custom'){
        actualDir = './public/editor/templates/custom/';
    }
    
    htmlToFileScr.ProcessReqData(req, actualDir);
    res.end();

});


// getting data from client and saving to a file
app.post('/loadTemplate', function (req, res) {



    fs.readdir('./public/editor/templates/custom/', function(err, items) {


        var onlyHtml = [];

        items.forEach(function(item, items) {
            if(item.split('.')[1] == 'html'){
                // console.log(item);
                onlyHtml.push(item);
            }


        });


        var jsonData=onlyHtml;
        res.send(jsonData);
    });
});




// getting a list of templates
app.post('/getTemplatesForTabs', function (req, res) {

    var templTabId = req.body.templateTabId;
    var actualDir;
    if(templTabId === '#editor_presetList1'){
        actualDir = './public/editor/templates/bootstrapBasic/';
    }else if(templTabId === '#editor_presetList2'){
        actualDir = './public/editor/templates/components/';
    }else if(templTabId === '#editor_presetList3'){
        actualDir = './public/editor/templates/custom/';
    }
    
    fs.readdir(actualDir, function(err, items) {
        
        var onlyHtml = [];

        items.forEach(function(item, items) {
            if(item.split('.')[1] === 'html'){
                onlyHtml.push(item);
            }
           
        });

        res.send(onlyHtml);
    });
   

});






// reading from a file
app.post('/sendTemplateName', function(req, res) {

    // 'templateTab':template_tab

    var templTab = req.body.templateTab;
    var folder;
    if(templTab === 'basic'){
        folder = 'bootstrapBasic';
    }else if(templTab === 'more'){
        folder = 'components';
    }else if(templTab === 'custom'){
        folder = 'custom';
    }



    var htmlFile = "public/editor/templates/" + folder +'/' + req.body.templateName +".html";
    var cssFile = "public/editor/templates/" + folder +'/' + req.body.templateName +".css";


    var htmlContent = '';
    var cssContent = '';
    var cssLinkContent = '';

    if(checkPathScr.FileExists(htmlFile)) {
        htmlContent = readFromFileScr.Read("public/editor/templates/" + folder +'/' + req.body.templateName + ".html");
    }
        
    if(checkPathScr.FileExists(cssFile)){
        cssContent = readFromFileScr.Read("public/editor/templates/" + folder +'/'+ req.body.templateName +".css");
        cssLinkContent = "<link rel='stylesheet' type='text/css' href='/templates/" + folder +'/' + req.body.templateName + ".css'/>";
    }


    var content =
    {
        htmlVal:htmlContent,
        cssVal:cssContent,
        cssLink:cssLinkContent
        
    };
    
    res.send(content);

    
});






var server = app.listen(8080, function () {

    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);

});