
selectorsListingMain();


function selectorsListingMain(){

    onKeyUpUpdate();
    highlightConnectedSelector();
    contextAddButton();
    addSelectorUI();
    addSelector();
    chooseSelector();
    removeSelector();
}


function onKeyUpUpdate(){

    $(document).keyup(function(event) {


        getSelectorsList();


    });
}


function getSelectorsList() {


    // checks
    var actualTab = getTabId(template_tab);
    var allItems = $('#' + actualTab).children();

    if(
        allItems.length === 0 ||
        isHighlighted
    ){
        return;
    }

    var cssStr = $('#editor_cssCode').val();
    var rulesArr = parseCSSInput(cssStr);

    $('#selectors_list').empty();




    for (var i = 0; i < rulesArr.length; i++) {

        var delImage = '<img src="delete.png" class="selectors_delImage"/>';
        var selItem = '<p class="selectorItem">' + rulesArr[i].selectorKey +'</p>'+ delImage;


        $('#selectors_list').append(selItem);

    }


}

function highlightConnectedSelector() {

    var actualSelector;
    var actualSelectors;


    $(document).on('selectionChangedEvent', function(event) {

        actualSelector = '';
        actualSelectors = [];


        $('.highest_priority').removeClass('highest_priority');
        $('.highest_priority2').removeClass('highest_priority2');
        $('#selectors_list').scrollTop(0);
        // console.log("--------------");


        if(selectedTarget === '' ||
            selectedTarget === undefined
        ){
            return;
        }

        
        ////////////////////////////////////////////////////////////////////////

        var idOfElem = $(selectedTarget).attr('id');

        ///*******************************************************************//

        var classesOfElemStr = $(selectedTarget).attr('class');
        
        classesOfElemStr = noTechClassesString(classesOfElemStr);


        // tempStr for checking if it's an empty string but with some white spaces
        var tempStr = classesOfElemStr.replace(/ /g,'');
        // preparing classes array for check
        var classesOfElemArr = [];
        if(isValue(tempStr)){
            classesOfElemArr = classesOfElemStr.split(' ');
        }


        ///*******************************************************************//

        var tagOfElem = $(selectedTarget).prop('tagName').toLowerCase();

        ////////////////////////////////////////////////////////////////////////



        if(isValue(idOfElem)){
            checkId(idOfElem);
            // console.log("id..");
        }else if(isValue(classesOfElemArr )){
            // console.log("class..");
            checkClass(classesOfElemArr);
        }else if(isValue(tagOfElem)){
            // console.log("tag..");
            checkTag(tagOfElem);

    }



        if(isValue(actualSelector)){

            activateItem();
            cssSelector = $(actualSelector).text();
            // console.log("cssSelector -- " + cssSelector);

        }else{
            $('.highest_priority').removeClass('highest_priority');
            $('.highest_priority2').removeClass('highest_priority2');
            $('#selectors_list').scrollTop(0);

            cssSelector = '';
            // console.log("-- no selector --");
        }




        if(actualSelectors.length > 0){

            activateClasses();

        }


    });




    function checkId(idOfElem){




        var items = $('#selectors_list').children();

        for (var i = 0; i < items.length; i++) {

            var strToCheck = $(items[i]).text();
            if (strToCheck === '#' +idOfElem) {

                actualSelector = items[i];
                return;
            }
        }

        actualSelector = '';

    }











    function checkClass(classesOfElemArr){


        var items = $('#selectors_list').children();

        for (var i = 0; i < items.length; i++) {
            var str1 = $(items[i]).text();
            if(str1 === '')
                continue;


            str1 = str1.split('.')[1];
            // console.log("str1 -- " + str1);
            for (var a = 0; a < classesOfElemArr.length; a++) {

                var str2 = classesOfElemArr[a];

                if (str1 === str2) {
                    // console.log("str1 -- " + str1);
                    // console.log("str2 -- " + str2);
                    actualSelector = items[i];
                    actualSelectors.push(items[i]);
                    // return;
                }
            }
        }



    }









    function checkTag(tagOfElem){


        var items = $('#selectors_list').children();

        for (var i = 0; i < items.length; i++) {

            var strToCheck = $(items[i]).text();

            if (strToCheck === tagOfElem) {
                actualSelector = items[i];
                return;
            }else if(strToCheck.indexOf(' ' + tagOfElem) !== -1){
                var parentEl = strToCheck.split(' ')[0];

                if(hasParent(parentEl)){
                    actualSelector = items[i];
                    return;
                }
            }
        }

        actualSelector = '';

    }



    function hasParent(searchedParent){


        var items = $(selectedTarget).parents();



        for (var i = 0; i < items.length; i++) {



            if ($(items[i]).attr('id') === 'editor_page-area'){

                return false;
            }




            if(searchedParent.indexOf('.') !== -1){


                if($(items[i])[0].hasAttribute("class")){

                    var searchedParentClass = searchedParent.split('.')[1];
                    var selElemClass = $(items[i]).attr('class');
                    var selElemClassArr = selElemClass.split(' ');

                    for (var a = 0; a < selElemClassArr.length; a++) {
                        if(selElemClassArr[a] === searchedParentClass) {
                            return true;
                        }
                    }

                }




            }else if(searchedParent.indexOf('#') !== -1){


                if($(items[i])[0].hasAttribute("id")){

                    var searchedParentId = searchedParent.split('#')[1];
                    var selElemId = $(items[i]).attr('id');

                    if(selElemId === searchedParentId) {
                        return true;
                    }
                }


            }



        }


    }




    function activateItem(){

        $(actualSelector).addClass('highest_priority');
        $('#selectors_list').scrollTop($(actualSelector).offset().top - 750);

    }


    function activateClasses(){

        for (var i = 0; i < actualSelectors.length; i++) {
            $(actualSelectors[i]).addClass('highest_priority');
        }

        $('#selectors_list').scrollTop($(actualSelectors[0]).offset().top - 750);

    }



}





// creating custom event to use when selected element changed
function selectionChangedEventRegister() {


    $.event.trigger({
        type: "selectionChangedEvent"
    });


}




function contextAddButton(){

    $(document).on("contextmenu",  function(event) {

        // just preventing on the section
        if($(event.target).attr('class') === 'context_new'

        ) {

            event.preventDefault();

        }



        if($(event.target).attr('id') === 'selectors_list' ||
            $(event.target).attr('class') === 'selectorItem') {

            event.preventDefault();

            // Show contextmenu
            $(".custom-menu").finish().toggle(100).

            // In the right position (the mouse)
            css({
                top: (event.pageY - 40) + "px" ,
                left: (event.pageX - 50) + "px"
            });



            // If the document is clicked somewhere
            $(document).on("mousedown", function (e) {

                // If the clicked element is not the menu
                if (!$(e.target).parents(".custom-menu").length > 0) {

                    // Hide it
                    $(".custom-menu").hide(100);
                }
            });

        }



    });


}


function addSelectorUI(){

    $(document).on("mousedown", function (event){
        // $('.context_new').on('mousedown', function () {

        // showing #addSelectorWindow
        if($(event.target).attr('class') === 'context_new'){


            $('#addSelectorWindow').show(100);
            $(".custom-menu").hide(100);


            // hiding #addSelectorWindow
        }else{


            var hasPar = hasParent(event.target, 'addSelectorWindow');

            if(!hasPar){
                $('#addSelectorWindow').hide(100);
            }
        }





    });


}



function addSelector() {


    $('.addSelBtn').on('mousedown', function(event) {

        addSelectorProcessing();

    });



    $(document).on('keypress', function(event) {


        if(event.keyCode == '13' && $('#addSelectorWindow').css('display') !== 'none'){

            addSelectorProcessing();

        }

    });


    function addSelectorProcessing(){


        if($('.textField').val() === ''){
            $('#addSelectorWindow').hide(100);
            return;
        }

        var selectorsArr = parseCSSInput($('#editor_cssCode').val());


        var rule = {
            selectorKey: '.' + $('.textField').val(),
            propertiesKey: ''
        };


        selectorsArr.push(rule);


        var updatedCss = assembleRulesOutput(selectorsArr);

        $('#editor_cssCode').val(updatedCss);
        UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());
        getSelectorsList();
        $('#addSelectorWindow').hide(100);


    }


}






// processing selecting actual selector among available selectors
function chooseSelector(){


    $(document).on('mousedown', function(event) {


        if($(event.target).hasClass('highest_priority')){

            // gathering all available selectors
            var actuals = [];
            if($('.highest_priority').length > 0)
                actuals.push($('.highest_priority'));

            if($('.highest_priority').length > 0)
                actuals.push($('.highest_priority2'));


            // changing color of selected and returning color of unselected
            $.each(actuals, function( index, value ) {

                if($(event.target).text() === $(value).text()){

                    $('.highest_priority').removeClass('highest_priority');
                    $(event.target).addClass('highest_priority2');
                }else{

                    $('.highest_priority2').removeClass('highest_priority2');
                    $(event.target).addClass('highest_priority');
                }



            });

            // assigning text value of actual selector for further usage
            cssSelector = $(event.target).text();
            $(event.target).addClass('highest_priority2');

        }



    });


}



function removeSelector(){


    // selectors_delImage
    $(document).on('click', function (event) {

        if(!$(event.target).hasClass('selectors_delImage')){
            return;
        }

        var selectorName =$(event.target).prev().text();


        cssDB = parseCSSInput($('#editor_cssCode').val());

        for (i = 0; i < cssDB.length; i++) {


            if (cssDB[i].selectorKey === selectorName) {
                console.log("cssDB[i].selectorKey -- " + cssDB[i].selectorKey);
                console.log("selectorName -- " + selectorName);
                cssDB.splice(i, 1);
                break;
            }
        }

        var updatedCss = assembleRulesOutput(cssDB);

        $('#editor_cssCode').val(updatedCss);
        UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());


        if($(event.target).hasClass('selectors_delImage')){

            $(event.target).prev().remove();
            $(event.target).remove();
        }




    });

}