

// вставляем подстроку в определённый индекс
function insert(str, index, value) {
    return str.substr(0, index) + value + str.substr(index);
}

// greate function to bound value within a range
function value_limit(val, min, max) {
    return val < min ? min : (val > max ? max : val);
}



function roundToStep(input, step){
    return Math.round(input / step) * step;
}

// удаляем из html тэг <style> и всё что внутри него
function removeStyleTag(str){

    var tag1 = str.indexOf("<style>");
    var tag2 = str.indexOf("</style>");

    var firstPart = str.slice(0 , tag1);
    var secondPart = str.slice(tag2+8 , str.length);

    return firstPart + secondPart;

}

// work with event.target
function getFullElemName(selectedTarget){

    var idVal = '';
    var classVal = '';

    if(selectedTarget.id != '')
        idVal = "#" + selectedTarget.id;

    if(selectedTarget.className != ''){
        classVal = "." + selectedTarget.className;
        classVal = classVal.replace(" ", ".");
    }

    return selectedTarget.nodeName.toLowerCase() + idVal + classVal;

}







// work with jquery's object - $(event.target)   -- better use this
function getFullElemNameFromSelector(selector){


    var selName = selector.prop('tagName').toLowerCase();

    if(selector.prop('tagName') === undefined)
        return undefined;

    if(selector.attr('id') !== undefined && selector.attr('id').length > 0)
        selName += "#" + selector.attr('id');

    if(selector.attr('class') !== undefined && selector.attr('class').length > 0){

        var classStr = $(selector).attr('class');

        classStr = noTechClassesString(classStr);
        if(isValue(classStr)){

            var arr = classStr.split(' ');
            var outputStr = '';

            for (var a = 0; a < arr.length; a++) {

                if(arr[a] === '')
                    continue;

                outputStr += '.' + arr[a];
            }
            selName += outputStr;

        }


    }


    return selName;

}





function noTechClassesString(classStr){

    classStr = classStr.replace(/techID_\d*/, "");
    classStr = classStr.replace(/display_on/, "");
    classStr = classStr.replace(/display_off/, "");
    classStr = classStr.replace(/closed/, "");
    classStr = classStr.replace(/outer_display_off/, "");
    classStr = classStr.replace(/highlighted/, "");

    return classStr;

}


function techClassesString(classStr){
    


    var classesArr = classStr.split(' ');
    var outputStr = '';

    for (var a = 0; a < classesArr.length; a++) {

        if (classesArr[a].indexOf('techID_') !== -1 ||
            classesArr[a].indexOf('display_on') !== -1 ||
            classesArr[a].indexOf('display_off') !== -1 ||
            classesArr[a].indexOf('closed') !== -1 ||
            classesArr[a].indexOf('outer_display_off') !== -1 ||
            classesArr[a].indexOf('highlighted') !== -1
        ) {
            outputStr = outputStr + ' ' + classesArr[a];
        }
    }
    return outputStr;
}





function updateElementName(){
    // console.log("$(selectedTarget).prop('tagName') -- " + $(selectedTarget).prop('tagName'));
    if($(selectedTarget).prop('tagName') === undefined){
        return;
    }
    var tempStr = getFullElemNameFromSelector($(selectedTarget));


    $('#elementName').text(tempStr);

}







function parseCSSInput(CSSInput) {
    var rulesTextBox = [];

    var firstBracesIndexes = getAllIndexes(CSSInput, "{");
    var secondBracesIndexes = getAllIndexes(CSSInput, "}");


    for (var i = 0; i < firstBracesIndexes.length; i++) {

        var selectorTextBox = '';
        var propertiesTextBox = [];


        if (i === 0) {
            selectorTextBox = CSSInput.substring(0, firstBracesIndexes[i]).trim();
            // console.log(selectorTextBox);
        } else {
            selectorTextBox = CSSInput.substring(secondBracesIndexes[i - 1] + 1, firstBracesIndexes[i]).trim();
            // console.log(selectorTextBox);
        }

        var tempProps = CSSInput.substring(secondBracesIndexes[i], firstBracesIndexes[i] + 1).split(";");



        for (var a = 0; a < tempProps.length; a++) {
            if (tempProps[a].match(/[a-z]/gi) != null) {
                propertiesTextBox.push(tempProps[a].trim());

            }

        }

        var ruleTextBox = {
            selectorKey: selectorTextBox,
            propertiesKey: propertiesTextBox
        };


        rulesTextBox.push(ruleTextBox);
    }

    return rulesTextBox;
}






function assembleRulesOutput(rulesArray) {


    var rule = "";
    var rulesString = "";



    for (var a = 0; a < rulesArray.length; a++) {

        var propsString = "";
        rule = rulesArray[a]["selectorKey"];

        // console.log("rule -- " + rule);

        var props = rulesArray[a]["propertiesKey"];

        // console.log("props -- " + props);

        if (props.length == 1) {
            rulesString = rulesString + "\n" + rule + "\n" + "{" + "\n\t" + props[0] + ";" + "\n" + "}";
        } else {
            for (var b = 0; b < props.length; b++) {
                propsString = propsString + '\t' + props[b] + ";" + "\n";
            }
            rulesString = rulesString + "\n" + rule + "\n" + "{" + "\n" + propsString + "}";
        }
    }

    return rulesString;
}



function mergeStyle(cssDB, stylesToCheck, currEditedRule) {

    var propsCheck = 0;
    var selectorsCheck = 0;

    var newSelector = {

        propertiesKey: stylesToCheck,
        selectorKey: cssSelector

    };

    cssDB.forEach(function (item, a, cssDB) {

        if (item.selectorKey === cssSelector) {

            var props = item.propertiesKey;
            propsCheck = 0;

            props.forEach(function (oldStyle, b, props) {


                var oldStyleCheck = oldStyle.split(':')[0];
                var newStyleCheck = currEditedRule.split(':')[0];

                if (oldStyleCheck === newStyleCheck) {

                    props[b] = currEditedRule;
                    propsCheck++;
                }

            });

            if (propsCheck === 0) {
                props.push(currEditedRule);
            }
            selectorsCheck++;
        }

    });


    if (selectorsCheck === 0) {
        cssDB.push(newSelector);
    }


}




// удаляем дубликаты элементов массива
function unique(arr) {
    var result = [];

    nextInput:
        for (var i = 0; i < arr.length; i++) {
            var str = arr[i]; // для каждого элемента
            for (var j = 0; j < result.length; j++) { // ищем, был ли он уже?
                if (result[j] == str) continue nextInput; // если да, то следующий
            }
            result.push(str);
        }

    return result;
}







// this finds and return indexes of of occurences in a string
function getAllIndexes(arr, val) {
    var indexes = [],
        i;
    for (i = 0; i < arr.length; i++)
        if (arr[i] === val)
            indexes.push(i);
    return indexes;
}







function validateClickArea(searchedParent, tarToCheck){

    var isValidArea = false;


    if($(tarToCheck).attr('id') === searchedParent){

        isValidArea = true;
    }else{
        $.each($(tarToCheck).parents(), function () {

            if ($(this).attr('id') === searchedParent) {

                isValidArea = true;
            }

        });
    }


    return isValidArea;


}





function UpdateHtmlCssChanges(textVal, cssVal){

    var currCss = "<style>" + cssVal + "</style>";
    $("#editor_page-area").html(textVal + currCss);
    isHighlighted = false;

}







// update html editor
function updateHtmlEditor(){

    var str = $("#editor_page-area").html();
    str = beautifyString(str);
    str = removeStyleTag(str);
    $("#editor_htmlCode").val(str);
    return str;

}





function isValue(val) {

    if(
        val === undefined ||
        val === '' ||
        val === null

    ){

        return false;

    }else if(val.length === 0)
    {
        return false;

    }else{

        return true;

    }



}








// check if element has given parent
function hasParent(elem, parentIdOrClass){


    var parentsOfTarget = $(elem).parents();
    var hasPar = false;

    for (var i = 0; i < parentsOfTarget.length; i++) {

        if($(parentsOfTarget[i]).attr('id') === parentIdOrClass ||
            $(event.target).attr('id') === parentIdOrClass

        ){

            hasPar = true;
            break;

        }else if($(parentsOfTarget[i]).attr('class') === parentIdOrClass ||
            $(event.target).attr('class') === parentIdOrClass

        ){

            hasPar = true;
            break;

        }
    }


    return hasPar;

}











// caching variables for context menu
function defineVars(){

    var varsObj = {

        display_group: $('.display_group'),
        float_group: $('.float_group'),
        vertical_align_group: $('.vertical_align_group'),
        text_align_group: $('.text_align_group'),
        step_section: $('#step_section'),
        value_type: $('#value_type'),
        marg_pad: $('.marg_pad'),
        marg_pad_auto: $("label[for='marg_pad_auto']"),
        one: $("label[for='one']"),
        marg_pad_pixel: $("label[for='marg_pad_pixel']"),
        left: $("label[for='left']")

    };



    return varsObj;
}











/////////////////////////////// --- plugins --- ///////////////////////////////

// plugin providing ability to change tag name
(function($){
    var $newTag = null;
    $.fn.tagName = function(newTag){
        this.each(function(i, el){
            var $el = $(el);
            $newTag = $("<" + newTag + ">");

            // attributes
            $.each(el.attributes, function(i, attribute){
                // console.log(attribute);
                $newTag.attr(attribute.nodeName, attribute.nodeValue);
            });
            // content
            $newTag.html($el.html());

            $el.replaceWith($newTag);
        });
        return $newTag;
    };
})(jQuery);




// plugin for replacing classes
(function ($) {
    $.fn.replaceClass = function (pFromClass, pToClass) {
        return this.removeClass(pFromClass).addClass(pToClass);
    };
}(jQuery));


/////////////////////////////// --- plugins --- ///////////////////////////////



// this updates style tag on current page with css editor value
function updateStyleTag(){



    $('style').html($('#editor_cssCode').val());

}


// snippet for converting multistring /////////////////////////////

// var multilineString	= (function(){ /*
//
//  multistring
//
//  */}).toString().split('\n').slice(1, -1).join('\n');
//
///////////////////////////////////////////////////////////////////






