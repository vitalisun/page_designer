
// script process mouse editing css

mainCssEdit();
textAlign();
setMeasureType();



function mainCssEdit() {



    $(document).on('mousedown', function(event){



        var tarToCheck = event.target;

        if(
            validateClickArea('editor_left-panel', tarToCheck) ||
            validateClickArea('editor_right-panel', tarToCheck) ||
            validateClickArea('editor_editor-pane', tarToCheck)
        ){
            return;
        }


        if(validateClickArea('editor_editor-pane', tarToCheck)){

            UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());

        }

        // init variables
        startX = event.pageX;

        // checks
        if(
            event.shiftKey || mode !== 'css_edit'  ||
            $('.highlighted').length === 0 ||
            $('.tool_btn.ui-state-active').attr('for') === undefined ||
            $('.tool_btn.ui-state-active').attr('for') === 'display'
        ){
            return;
        }



        var currTool = $('.tool_btn.ui-state-active').attr('for');
        var dirPart = $('.directions.ui-state-active').attr('for');


        if(dirPart === undefined || dirPart === 'all'){
            cssRule = currTool;
        }else{
            cssRule = currTool + '-' + dirPart;
        }



        if($(cssSelector).css(cssRule) !== undefined){

            currCssVal = $(cssSelector).css(cssRule);
            var bodyVal;

            if(cssRule.indexOf('top') !== -1 || cssRule.indexOf('bottom') !== -1){
                bodyVal = $('#editor_page-area').css('height');
            }else if(cssRule.indexOf('left') !== -1 || cssRule.indexOf('right') !== -1 || cssRule.indexOf('width') !== -1 ){
                bodyVal = $('#editor_page-area').css('width');
            }else if(currTool === 'height'){
                bodyVal = $('#editor_page-area').css('height');
            }


            currCssValInt = parseInt(currCssVal, 10);
            currCssValParentInt = parseInt(bodyVal, 10);

            if(measureType === '%' && prevCssValFull.indexOf('%') === -1){
                // getting current percent

                currCssVal = (currCssValInt/currCssValParentInt)*100;

            }else if(measureType === '%' && prevCssValFull.indexOf('%') !== -1){

                if(prevCssRule === cssRule){

                    currCssVal = parseInt(prevCssValFull, 10);

                }else{
                    currCssVal = (currCssValInt/currCssValParentInt)*100;
                }

            }



        }






        $(document).mousemove(function(event){


            applyCSS(event, currCssVal, startX, cssRule);


        });




        $(document).mouseup(function(event){

            // checks
            // if element has no style then break execution
            if(currCssVal === undefined || cssRule === undefined){

                customError = "info -- element has no style";
                return;
            }

            // check if selector is assigned

            if(cssSelector === ''){

                customError = "info -- assign name of seletor";
                return;
            }

            // console.log("currEditedRule -- " + currEditedRule);
            prevEditedRule = currEditedRule;
            prevCssRule = cssRule;

            $(document).unbind('mousemove');
            $(document).unbind('mouseup');

            // css for current page to store in object array
            cssDB = parseCSSInput($('#editor_cssCode').val());


            var currStyle = $(cssSelector).attr('style');
            var arr;

            try{
                arr = currStyle.split(';');
            }
            catch(e) {
                console.log(customError);
                return;
            }


            ////////////    a bit of formatting    ////////////
            var newStr = '';

            // array if selected styles for comparing with css of the page
            stylesToCheck = [];

            arr.forEach(function(item, i, arr) {
                // console.log(item);
                item = item.replace(' ', '');

                if(item.length > 0){
                    // console.log("item -- " + item);
                    stylesToCheck.push(item);
                    newStr += '\t' + item + ';\n';
                }

            });


            currStyle = newStr;

            ////////////    *****************    ////////////


            // saving style for edited element
            styleOutput = cssSelector +
                '{\n' + currStyle + '}';


            mergeStyle(cssDB, stylesToCheck, currEditedRule);

            $('#editor_cssCode').val(assembleRulesOutput(cssDB));

            // get update page
            UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());

            // removing unnecessary rules like padding-left if we have padding at the end
            clearMargPad();
        });


    });

}


function applyCSS(event, currCssVal, startX, cssRule){


    // if element has no style then break execution
    if(currCssVal === undefined || cssRule === undefined){

        customError = "info -- element has no style";
        return;
    }


    // check if selector is assigned

    if(cssSelector === ''){

        customError = "info -- assign name of seletor";
        return;
    }


    var finalCssVal = parseInt(currCssVal, 10) + event.pageX - startX;


    // processing step
    var stepLabels = $('.step');
    for (i = 0; i < stepLabels.length; i++) {
        if($(stepLabels[i]).hasClass('ui-state-active')){
            var stepVal = $(stepLabels[i]).text();
            finalCssVal = roundToStep(finalCssVal, stepVal);
        }

    }

    // processing negative percent
    if(measureType === '%' && finalCssVal < 0) {
        finalCssVal = 0;
    }else if(
        (
            cssRule.indexOf('padding') !== -1 ||
            cssRule.indexOf('width') !== -1 ||
            cssRule.indexOf('height') !== -1
        )
        && finalCssVal < 0 )
    {
        finalCssVal = 0;
    }



    // processing auto
    if(measureType === 'auto'){
        finalCssVal = 'auto';
        currEditedRule = cssRule + ": " + finalCssVal;
        $(cssSelector).css(cssRule, finalCssVal);

    }else{
        currEditedRule = cssRule + ": " + finalCssVal + measureType;
        // console.log("currEditedRule -- " + currEditedRule);
        $(cssSelector).css(cssRule, finalCssVal + measureType);
        prevCssValFull = finalCssVal + measureType;
    }




}






function textAlign(){

    $('#align_left').on('click', function () {
        $(selectedTarget).addClass('text-left');
        $(selectedTarget).removeClass('text-center');
        $(selectedTarget).removeClass('text-right');

        // updating element name display
        updateElementName();

    });

    $('#align_center').on('click', function () {
        $(selectedTarget).addClass('text-center');
        $(selectedTarget).removeClass('text-left');
        $(selectedTarget).removeClass('text-right');

        // updating element name display
        updateElementName();

    });

    $('#align_right').on('click', function () {
        $(selectedTarget).addClass('text-right');
        $(selectedTarget).removeClass('text-center');
        $(selectedTarget).removeClass('text-left');

        // updating element name display
        updateElementName();

    });

}


// measureType


function setMeasureType(){

    $('label').on('click', function () {

        var forAttr = $(this).attr('for');


        if(forAttr === 'marg_pad_pixel'){
            measureType = 'px';
        }else if(forAttr === 'marg_pad_percent'){
            measureType = '%';
        }else if(forAttr === 'marg_pad_auto'){
            measureType = 'auto';
        }


    });


}

// this removes rules like padding-left if we have padding at the end of style
function clearMargPad() {

    var cssDBlocal = parseCSSInput($('#editor_cssCode').val());

    for (i = 0; i < cssDBlocal.length; i++) {


        // console.log("item.selectorKey -- " + item.selectorKey);
        // console.log("$('.highest_priority').text() -- " + $('.highest_priority').text());

        if (cssDBlocal[i].selectorKey === $('.highest_priority').text()) {

            var props = cssDBlocal[i].propertiesKey;


            for (b = 0; b < props.length; b++) {

                var rule = props[b].split(':')[0];

                if(rule === 'margin' || rule === 'padding'){

                    var newArr = [];

                    for (c = 0; c < props.length; c++) {

                        var rule2 = props[c].split(':')[0];


                        if(rule2.indexOf(rule) !== -1 &&
                            rule2 !== rule
                        ){

                            continue;
                        }else{

                            newArr.push(props[c]);

                        }
                    }


                    if(newArr.length > 0){
                        cssDBlocal[i].propertiesKey = newArr;
                    }
                    var updatedCss = assembleRulesOutput(cssDBlocal);
                    $('#editor_cssCode').val(updatedCss);
                    UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());


                }


            }





        }

    }



}

