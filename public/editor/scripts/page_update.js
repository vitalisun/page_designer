
// updating the page
page_update_main();


// this update works in template mode so that we can change or correct code and get an updated page
function page_update_main(){

    $('#editor_page-area').on('click', function(event){



        var actualTab = getTabId(template_tab);
        var presets = $('#' + actualTab).children();

        if (event.shiftKey ||
            presets.length === 0 ||
            mode !== 'template_edit' ||
            $(event.target).attr('id') === 'textEditInput'
        ) {
            return;
        }



        // updating name of selected element on top menu
        $('.highlighted').removeClass('highlighted');
        selectedTarget = '';
        $('#elementName').text('no element selected');
        selectionChangedEventRegister();  // registering custom event
        // updating the page itself
        updateHtmlEditor();
        UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());




    });

}

