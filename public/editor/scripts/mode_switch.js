

// working with mode
modeSwitchMainFunction();
getEditedRule();


function modeSwitchMainFunction(){

    changeMode();
    activateDefaultsForCurrentMode(mode);
    displayMode();

}



function changeMode() {


    $('#showModeDiv').on( "click", function(event) {

        mode = $('#showMode').text();

        if(mode === 'template_edit'){

            $('#showMode').text('css_edit');
            mode = 'css_edit';

        }else if(mode === 'css_edit'){

            $('#showMode').text('grid_edit');
            mode = 'grid_edit';

        }else if(mode === 'grid_edit'){

            $('#showMode').text('template_edit');
            mode = 'template_edit';

        }

        activateDefaultsForCurrentMode(mode);

    });


}




function displayMode(){

    $('#showMode').text(mode);

}










function activateDefaultsForCurrentMode(mode){

    var obj = defineVars();


    for (var prop in obj) {

        $(obj[prop]).hide();

    }

    $('.ui-state-active').removeClass('ui-state-active');

    switch (mode) {
        case 'template_edit':
            $('#elementName').text('no element selected');
            $('#editedRule').text('---');


            $(obj['one']).show();
            $(obj['marg_pad_pixel']).show();
            $(obj['left']).show();
            $(obj['step_section']).show();
            $(obj['value_type']).show();
            $(obj['marg_pad']).show();
            $(obj['marg_pad_auto']).show();

            $(obj['one']).addClass('ui-state-active');
            $(obj['marg_pad_pixel']).addClass('ui-state-active');
            $(obj['left']).addClass('ui-state-active');

            // turning off other defaults
            $('.highlighted').removeClass('highlighted');
            isHighlighted = false;
            $('#collapse5').removeClass("in");
            $('#collapse5').attr("style","height: 0px;");
            $('#collapse5').attr("aria-expanded",false);
            break;

        case 'css_edit':


            // turning on css_edit defaults

            $(obj['one']).show();
            $(obj['marg_pad_pixel']).show();
            $(obj['left']).show();
            $(obj['step_section']).show();
            $(obj['value_type']).show();
            $(obj['marg_pad']).show();
            $(obj['marg_pad_auto']).show();



            $("label[for='margin']").addClass('ui-state-active');
            $("label[for='one']").addClass('ui-state-active');
            $("label[for='marg_pad_pixel']").addClass('ui-state-active');
            $("label[for='left']").addClass('ui-state-active');

            $('#collapse5').addClass("in");
            $('#collapse5').attr("style","");
            $('#collapse5').attr("aria-expanded",true);


            getInfo();
            break;

        case 'grid_edit':


            // turning off css_edit defaults
            $('#collapse5').removeClass("in");
            $('#collapse5').attr("style","height: 0px;");
            $('#collapse5').attr("aria-expanded",false);

            // turning on grid_edit defaults
            $("label[for='md']").addClass('ui-state-active');
            break;

    }

}











function activateDefaultsForCurrentModeold(mode){



    switch (mode) {
        case 'template_edit':
            $('#elementName').text('no element selected');
            $('#editedRule').text('---');

            $('.marg_pad').hide();
            $('.display_group').hide();
            $('.float_group').hide();
            $('.vertical-align_group').hide();
            $('.text-align_group').hide();


            $('#text_context').show();
            $('#step_section').show();
            $('#value_type').show();
            // console.log("template_edit..");

            // turning off other defaults
            $('.highlighted').removeClass('highlighted');
            isHighlighted = false;
            $('.ui-state-active').removeClass('ui-state-active');
            $('#collapse5').removeClass("in");
            $('#collapse5').attr("style","height: 0px;");
            $('#collapse5').attr("aria-expanded",false);
            break;

        case 'css_edit':

            // turning off other defaults
            $('.ui-state-active').removeClass('ui-state-active');

            // turning on css_edit defaults

            $('.display_group').hide();
            $('.float_group').hide();
            $('.vertical-align_group').hide();
            $('.text-align_group').hide();

            $('#text_context').show();
            $('#step_section').show();
            $('#value_type').show();
            $('.marg_pad').show();


            $("label[for='margin']").addClass('ui-state-active');
            $("label[for='one']").addClass('ui-state-active');
            $("label[for='marg_pad_pixel']").addClass('ui-state-active');
            $("label[for='left']").addClass('ui-state-active');

            $('#collapse5').addClass("in");
            $('#collapse5').attr("style","");
            $('#collapse5').attr("aria-expanded",true);


            getInfo();
            break;

        case 'grid_edit':

            $('.marg_pad').hide();
            $('.display_group').hide();
            $('.float_group').hide();
            $('.vertical-align_group').hide();
            $('.text-align_group').hide();

            $('#text_context').hide();
            $('#step_section').hide();
            $('#value_type').hide();

            // turning off other defaults
            $('.ui-state-active').removeClass('ui-state-active');
            // turning off css_edit defaults
            $('#collapse5').removeClass("in");
            $('#collapse5').attr("style","height: 0px;");
            $('#collapse5').attr("aria-expanded",false);

            // turning on grid_edit defaults
            $("label[for='md']").addClass('ui-state-active');
            break;

    }

}



// measureType


function getEditedRule() {




    $(document).on('click', function (event) {

        if ($(event.target).hasClass('tool_btn') ||
            $(event.target).hasClass('ui-checkboxradio-label')
        ) {
            window.setTimeout( getInfo, 50 );
        }

    });

}



function getInfo(){


    var toolBtn = $('.tool_btn.ui-state-active').text();
    var dir = $('.directions.ui-state-active').text();


    if(toolBtn === 'margin' ||
        toolBtn === 'padding'
    ){
        // console.log("rule -- " + toolBtn + '-' + dir);
        $('#editedRule').text(toolBtn.replace('_', '-') + '-' + dir);
    }else{

        // console.log("rule -- " + toolBtn);
        $('#editedRule').text(toolBtn.replace('_', '-'));
    }

}