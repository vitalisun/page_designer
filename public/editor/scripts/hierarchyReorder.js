
hierarchyReorderMain();


function hierarchyReorderMain() {

    dragAndDropElement();

}




function dragAndDropElement(){





    $('#hierarchy').on('mousedown', '.chierarchyElemLabel', function(event) {


        var elem = $(event.target);
        var elemCopy = $(elem).clone();

        var startX = event.pageX;
        var startY = event.pageY;

        var closedSourceElem = false;


        $("body").append(
            elemCopy
        );

        $(elemCopy).css('position', 'absolute');
        $(elemCopy).css('z-index', 1000);

        var shiftX = event.pageX - $(elem).offset().left;
        var shiftY = event.pageY - $(elem).offset().top;

        $(elemCopy).css({

            'left':(event.pageX - shiftX) + 'px',
            'top':(event.pageY - shiftY) + 'px'

        });

        $(elem).addClass('chierarchyElemDragged');

        $(document).on('mousemove', function(event){

            if(Math.abs(startX - event.pageX) < 5 && Math.abs(startY - event.pageY) < 5)
                return;


            if(!closedSourceElem)
                closedSourceElem = closeSourceElem(elem);

            hierarchyMoveAt($(elemCopy), event);

            var target = getTarget(event, elemCopy);
            drawBorder(event, elem, target);

        });

        $(document).mouseup(function(event){

            if(Math.abs(startX - event.pageX) < 5 && Math.abs(startY - event.pageY) < 5){
                $(elem).removeClass('chierarchyElemDragged');
                $(document).unbind('mousemove');
                $(elemCopy).remove();
                $(document).unbind('mouseup');
                return;
            }


            var target = getTarget(event, elemCopy);

            if(isValue(target))
                processReorder(elem, target);

            $('.divisionLine').remove();
            $('.chierarchyElemHover').removeClass('chierarchyElemHover');
            $('.chierarchyElemNoHover').removeClass('chierarchyElemNoHover');
            $(elem).removeClass('chierarchyElemDragged');
            $(document).unbind('mousemove');
            $(elemCopy).remove();
            $(document).unbind('mouseup');

            updateHtmlEditor();
            UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());
            refreshHierarchy();

        });




        function hierarchyMoveAt(elem, event) {

            $(elem).css({

                'left':(event.pageX - shiftX) + 'px',
                'top':(event.pageY - shiftY) + 'px'

            });
        }

    });


}




function getTarget(event, elemCopy) {


    // hiding draggable elem
    $(elemCopy).hide();

    // getting position of cursor
    var x = event.clientX, y = event.clientY;
    var targetElement = document.elementFromPoint(x, y);

    $(elemCopy).show();


    return targetElement;


}




function drawBorder(event, elemSource, targetElement) {


    var elemCopyId = getIdOfParent(elemSource);
    var targetElementId = getIdOfParent(targetElement);

    var x = event.clientX, y = event.clientY;


    if(elemCopyId === targetElementId || targetElementId === undefined)
    {
        $('.divisionLine').remove();
        $('.chierarchyElemHover').replaceClass('chierarchyElemHover', 'chierarchyElemNoHover');
        return;
    }



    var topPos = $(targetElement).offset().top;
    var bottomPos = $(targetElement).offset().top + $(targetElement).innerHeight();
    var quarter = $(targetElement).innerHeight() / 4 ;


    var line = '<div class="divisionLine"></div>';


    if(y < bottomPos - quarter - 5  && y > topPos) {

        $(targetElement).replaceClass('chierarchyElemNoHover', 'chierarchyElemHover');

    }else if(y < bottomPos && y > bottomPos - quarter - 4){

        $('.divisionLine').remove();
        $($(targetElement).parent()).after(line);

    }
    else{
        $('.divisionLine').remove();
        $('.chierarchyElemHover').replaceClass('chierarchyElemHover', 'chierarchyElemNoHover');

    }


}









function closeSourceElem(source){


    var sourceElemId = getIdOfParent(source).replace('H_','.');
    $(sourceElemId).addClass('closed');
    processClosedClassInfluense($(sourceElemId), false);

    return true;
}










function processReorder(source, target){


    if(!isValue(getIdOfParent(target)))
        return;

    var sourceElemId = getIdOfParent(source).replace('H_','.');
    var targetElemId = getIdOfParent(target).replace('H_','.');

    // console.log("sourceElemId -- " + sourceElemId);
    // console.log("targetElemId -- " + targetElemId);

    // prevent operation on itself
    if(sourceElemId === targetElemId)
        return;


    var sourceTag = $(sourceElemId).prop('tagName').toLowerCase();
    var targetTag = $(targetElemId).prop('tagName').toLowerCase();

    // prevent wrong reorder
    if(sourceTag === 'div' && targetTag === 'a'){

        console.warn("invalid try - you can't put 'div' under a 'tag' !");
        return;
    }



    if($('.divisionLine').prop('tagName') !== undefined){
        $(targetElemId).after($(sourceElemId));

    }else{
        $(targetElemId).append($(sourceElemId));
        $(targetElemId).html($(targetElemId).children());
    }




    // console.log("www -- " + $(copyEl).attr('class'));

}








// getting ids of parent
function getIdOfParent(elem){



    if($(elem).parent().attr('class') === undefined)
        return;


    var parClasses = $(elem).parent().attr('class').split(' ');



    for(var a = 0; a < parClasses.length; a++){

        if(parClasses[a].indexOf('H_techID') !== -1){
            return parClasses[a];
        }

    }

}



