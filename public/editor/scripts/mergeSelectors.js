

// description
// this script merges selectors like:

//          .default_container
//          {
//               text-align: left;
//               width:5%;
//               color:white;
//          }
//
//      and
//
//          .default_container
//          {
//              width:auto;
//              text-align: right;
//          }

// function gets a string with name of selector with its properties
function mergeSelectorsMainFunction(str){

    var arr = parseCSSInput(str);
    
    var result = [];

    nextInput:
        for (var i = 0; i < arr.length; i++) {
            for (var j = 0; j < result.length; j++) {
                // удаляем дубликаты свойств
                result[j]['propertiesKey'] = unique(result[j]['propertiesKey']);

                if (result[j]['selectorKey'] == arr[i]['selectorKey']) {

                    var prop1 = arr[i]['propertiesKey'];
                    var prop2 = result[j]['propertiesKey'];
                    var propsConcated =  prop1.concat(prop2);
                    propsConcated = unique(propsConcated);
                    propsConcated = mergeProps(propsConcated);
                    result[j]['propertiesKey'] = propsConcated;
                    continue nextInput;
                }
            }
            result.push(arr[i]);
        }

    var outputStr = assembleRulesOutput(result.reverse());

    return outputStr;
}







// this merges properties like 'border-radius: 10px;' and 'border-radius: 22px;'
// collapcing into single 'border-radius' + the last '22px'
function mergeProps(propsArr){

    var propsObjsArr = [];

    // first make an objects array from string array
    for (var a = 0; a < propsArr.length; a++) {

        var obj = {
            property: propsArr[a].split(':')[0],
            value: propsArr[a].split(':')[1]
        };
        propsObjsArr.push(obj);
    }

    // here we get rid of dublicate properties leaving only new ones
    var result = [];

    nextInput:
        for (var b = 0; b < propsObjsArr.length; b++) {
            for (var c = 0; c < result.length; c++) {
                if (result[c]['property'] == propsObjsArr[b]['property']) {
                    continue nextInput;
                }
            }
            result.push(propsObjsArr[b]);
        }

    // now make an string array from objects array back
    var propsArrFinal = [];

    for (var d = 0; d < result.length; d++) {
        propsArrFinal.push(result[d]['property'] + ':' + result[d]['value']);
    }

    return propsArrFinal;
}






////////////////////    ////////////////////     ////////////////////    utilities      ////////////////////    ////////////////////     ////////////////////

function getAllIndexes(arr, val) {
    var indexes = [],
        i;
    for (i = 0; i < arr.length; i++)
        if (arr[i] === val)
            indexes.push(i);
    return indexes;
}




function unique(arr) {
    var result = [];

    nextInput:
        for (var i = 0; i < arr.length; i++) {
            var str = arr[i]; // для каждого элемента
            for (var j = 0; j < result.length; j++) { // ищем, был ли он уже?
                if (result[j] == str) continue nextInput; // если да, то следующий
            }
            result.push(str);
        }

    return result;
}




function parseCSSInput(CSSInput) {
    var rulesTextBox = [];

    var firstBracesIndexes = getAllIndexes(CSSInput, "{");
    var secondBracesIndexes = getAllIndexes(CSSInput, "}");


    for (var i = 0; i < firstBracesIndexes.length; i++) {

        var selectorTextBox = '';
        var propertiesTextBox = [];


        if (i === 0) {
            selectorTextBox = CSSInput.substring(0, firstBracesIndexes[i]).trim();
            // console.log(selectorTextBox);
        } else {
            selectorTextBox = CSSInput.substring(secondBracesIndexes[i - 1] + 1, firstBracesIndexes[i]).trim();
            // console.log(selectorTextBox);
        }

        var tempProps = CSSInput.substring(secondBracesIndexes[i], firstBracesIndexes[i] + 1).split(";");



        for (var a = 0; a < tempProps.length; a++) {
            if (tempProps[a].match(/[a-z]/gi) != null) {
                propertiesTextBox.push(tempProps[a].trim());

            }

        }

        var ruleTextBox = {
            selectorKey: selectorTextBox,
            propertiesKey: propertiesTextBox
        };


        rulesTextBox.push(ruleTextBox);
    }

    return rulesTextBox;
}


function assembleRulesOutput(rulesArray) {


    var rule = "";
    var rulesString = "";



    for (var a = 0; a < rulesArray.length; a++) {

        var propsString = "";
        rule = rulesArray[a]["selectorKey"];

        // console.log("rule -- " + rule);

        var props = rulesArray[a]["propertiesKey"];

        // console.log("props -- " + props);

        if (props.length == 1) {
            rulesString = rulesString + "\n" + rule + "\n" + "{" + "\n\t" + props[0] + ";" + "\n" + "}";
        } else {
            for (var b = 0; b < props.length; b++) {
                propsString = propsString + '\t' + props[b] + ";" + "\n";
            }
            rulesString = rulesString + "\n" + rule + "\n" + "{" + "\n" + propsString + "}";
        }
    }

    return rulesString;
}


///////////////////////////   for usage tests   ///////////////////////////

// var multilineString	= (function(){ /*
//
//  .default_container
//  {
//  width:auto;
//  text-align: center;
//  }
//
//  .default_container
//  {
//  width:30px;
//  text-align: center;
//  }
//
//  .button1
//  {
//  border-radius: 10px;
//  background-color: #bfbfbf;
//  width: 100px;
//  font-size:25px;
//  font-size:25px;
//  }
//  .button1
//  {
//  font-size:25px;
//  font-size:25px;
//  border-radius: 10px;
//  background-color: #bfbfbf;
//  width: 100px;
//  font-size:25px;
//  font-size:25px;
//  }
//
//  */}).toString().split('\n').slice(1, -1).join('\n');

// execute
// mergeSelectorsMainFunction(multilineString);

///////////////////////////   for usage tests   ///////////////////////////
