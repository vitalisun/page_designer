


mainSelect();
navigateHierarchy ();
deleteElement();
removeInlineStyle();
removeInlineStyleAll();


function mainSelect(){


    $(document).on('mousedown', function(event) {


        if(
            validateClickArea('editor_left-panel', event.target) ||
            validateClickArea('editor_right-panel', event.target) ||
            validateClickArea('editor_editor-pane', event.target) ||
            validateClickArea('editor_menu-panel', event.target) ||
            validateClickArea('editElementNameWindow', event.target) ||
            !event.shiftKey

        ){

            return;
        }


        // if((!event.shiftKey) && validateClickArea('editor_page-area', event.target)){
        //
        //     clearSelection();
        //
        // }


        // disable links
        if (event.target.nodeName === 'A') {
            $(event.target).attr("href", "javascript:void(0)");
        }

        selectedTarget = event.target;



        // saving target in case we need to get it back
        if ($(selectedTarget).length > 0) {

            actualTarget = selectedTarget;

            // calling click function
            var validArea = 0;
            $.each($(selectedTarget).parents(), function () {

                if ($(this).attr('id') === 'editor_page-area') {
                    validArea++;
                }

            });

            // getting back our target from arrows cause click was made outside of the valid area
            if (validArea > 0) {
                onClickHighlight(selectedTarget);
            } else {
                selectedTarget = actualTarget;
            }

        }





    });


}



// actual click processing
function onClickHighlight(selectedTarget){

    $('.highlighted').removeClass('highlighted');
    isHighlighted = false;
    $(selectedTarget).addClass('highlighted');
    isHighlighted = true;

    updateElementName();
    updateHtmlEditor();
    selectionChangedEventRegister();  // registering custom event

}












function clearSelection() {

    // updating name of selected element on top menu
    $('.highlighted').removeClass('highlighted');
    $('.chierarchyElemSelected').removeClass('chierarchyElemSelected');
    selectedTarget = '';
    $('#elementName').text('no element selected');
    selectionChangedEventRegister();  // registering custom event
    // updating the page itself
    updateHtmlEditor();
    UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());



}












function navigateHierarchy (){


    $(document).keydown(function (event) {


        if(!isHighlighted){
            return;
        }

        // event.preventDefault();
        switch (event.keyCode) {

            case 37:

                // console.log($(selectedTarget).prev().length);

                if($(selectedTarget).prev().length !== 0){

                    var newElem = $(selectedTarget).prev();
                    var nameElem = getFullElemNameFromSelector($(newElem));
                    // console.log("nameElem -- " + nameElem);
                    if(nameElem === 'style'){
                        // console.log("!!!");
                        break;
                    }

                    selectedTarget = newElem;
                    onClickHighlight(newElem);

                    // console.log("left");


                }
                break;

            case 38:



                if($(selectedTarget).parents().length > 0){

                    var newElem = $(selectedTarget).parent();
                    var nameElem = getFullElemNameFromSelector($(newElem));

                    // console.log("nameElem -- " + nameElem);
                    if(nameElem === 'div#editor_page-area' || nameElem === 'style'){
                        // console.log("!!!");
                        break;
                    }

                    selectedTarget = newElem;
                    onClickHighlight(newElem);

                    // console.log("up");

                }

                break;

            case 39:
                if($(selectedTarget).next().length !== 0){
                    var newElem = $(selectedTarget).next();
                    var nameElem = getFullElemNameFromSelector($(newElem));
                    // console.log("nameElem -- " + nameElem);
                    if(nameElem === 'style'){
                        // console.log("!!!");
                        break;
                    }

                    selectedTarget = newElem;
                    onClickHighlight(newElem);

                    // console.log("right");


                }

                break;

            case 40:

                if($(selectedTarget).children().length > 0){
                    var newElem = $(selectedTarget).children()[0];

                    selectedTarget = newElem;
                    onClickHighlight(newElem);

                    // console.log("down");
                }

                break;
        }

    });

}

function deleteElement(){

    $(document).keydown(function (event) {


        if(event.keyCode === 46){



            if(!isHighlighted){

                console.log(" nothing is selected ");
                return;
            }

            var isTopmostPar = false;
            var currEl = $(selectedTarget).parent();

            while(!isTopmostPar){

                // getting back our default bootstrap label text

                if($(currEl).hasClass('container')){

                    var kids = $(currEl).children();

                    if(kids.length > 1){
                        $(currEl).html($(currEl).children());
                    }else{
                        $(currEl).html('empty container');
                    }

                    break;

                }else if($(currEl).hasClass('row')) {

                    var kids = $(currEl).children();

                    if (kids.length > 1) {
                        $(currEl).html($(currEl).children());
                    } else {
                        $(currEl).html('empty row');
                    }

                    break;

                }else if($(currEl).hasClass('col3')) {

                    var kids = $(currEl).children();

                    if (kids.length > 1) {
                        $(currEl).html($(currEl).children());
                    } else {
                        $(currEl).html('col3');
                    }

                    break;

                }else{

                    var kids = $(currEl).children();

                    if (kids.length > 1) {
                        $(currEl).html($(currEl).children());
                    }

                    break;

                }


            }


            removeHierarchyElement($('.highlighted'));
            $('.highlighted').remove();
            updateHtmlEditor();


        }

    });

}








function removeInlineStyle(){

    $('.selectedElement').click(function(event) {


        if(!isHighlighted){

            console.log(" nothing is selected ");
            return;
        }

        $(selectedTarget).removeAttr( "style" );
        updateHtmlEditor();

    });


}









function removeInlineStyleAll(){

    $('.allElements').click(function(event) {


        var allDescendants = $("#editor_page-area").find("*");

        for (var i = 0; i < allDescendants.length; i++) {

            $(allDescendants[i]).removeAttr( "style" );

        }

        updateHtmlEditor();

    });


}



















