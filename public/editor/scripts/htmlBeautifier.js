
// html-beautifier улучшаем читабельность

function makeTagsArray(str){

    var i, a, b;

    var startIndex = 0;

    var tagArr = [];

    for(i = startIndex; i < str.length; i++){

        var openInd;
        var closeInd;
        var openInd2;

        if(str[i] === '<'){
            openInd = i;
            for(a = i; a < str.length; a++){
                if(str[a] === '>'){
                    closeInd = a;
                    var currStr = str.slice(openInd, closeInd + 1);

                    for(b = a; b < str.length; b++){
                        if(str[b] === '<'){
                            openInd2 = b;

                            currStr = str.slice(openInd, openInd2 );
                            break;
                        }
                    }

                    currStr = currStr.trim();


                    if(currStr.slice(1, 4) === 'img'){
                        tagArr.push(currStr);
                        tagArr.push('</img>');
                    }else{
                        tagArr.push(currStr);
                    }



                    break;

                }
            }
        }
    }

    return tagArr;
}

// обрабатываем массив - добавляем '\t' и '\n' для форматирования ввиде дерева 
function shiftTags(arr){

    var tabs = 0;
    var check = '';
    var prevCheck = '';
    var direction = '';

    var outPutStr = '';

    var i;
    

    for(i = 0; i < arr.length; i++){


        if(i > 0){
            prevCheck = arr[i-1].slice(0, 2);

        }

        check = arr[i].slice(0, 2);

        // console.log('prevCheck -- ' + prevCheck);
        // console.log('check -- ' + check);
        if(check !== '</'){
            if(prevCheck === '</' || prevCheck === ''){
                direction = 'up';
                // console.log(direction);
                tabs --;
            }
            tabs ++;
        }else if(check === '</'){
            if(prevCheck !== '</'){
                direction = 'down';
                // console.log(direction);
                tabs ++;
            }
            tabs --;
        }

        var formattedStr = "\t".repeat(Math.abs(tabs)) + arr[i] + '\n';
        // console.log(formattedStr);
        outPutStr += formattedStr;



    }



    return outPutStr;

}






function beautifyString(str){

    var arr = makeTagsArray(str);
    var outPut = shiftTags(arr);


    return outPut;
}








function getStyleLeft(elem) {

    var str = $(elem).attr('style');

    return parseInt(str.match(/[\d\.]+/));

}