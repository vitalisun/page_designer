
contextMenuUIMain();



function contextMenuUIMain() {
    "use strict";

    var obj = defineVars();
    proccessContext(obj);

}
















function proccessContext(obj){
    "use strict";

    $('label').on( "click", function(event) {

        var targetElem = $(event.target);

        if(! $(targetElem).hasClass('tool_btn'))
            return;



        for (var prop in obj) {

            $(obj[prop]).hide();

        }


        $('.ui-state-active').removeClass('ui-state-active');




        if(targetElem.attr('for') === 'margin'
        ){

            $(obj['one']).show();
            $(obj['marg_pad_pixel']).show();
            $(obj['left']).show();
            $(obj['step_section']).show();
            $(obj['value_type']).show();
            $(obj['marg_pad']).show();
            $(obj['marg_pad_auto']).show();

            $(obj['one']).addClass('ui-state-active');
            $(obj['marg_pad_pixel']).addClass('ui-state-active');
            $(obj['left']).addClass('ui-state-active');




        }else if(targetElem.attr('for') === 'padding'
        ){

            $(obj['one']).show();
            $(obj['marg_pad_pixel']).show();
            $(obj['left']).show();
            $(obj['step_section']).show();
            $(obj['value_type']).show();
            $(obj['marg_pad']).show();

            $(obj['one']).addClass('ui-state-active');
            $(obj['marg_pad_pixel']).addClass('ui-state-active');
            $(obj['left']).addClass('ui-state-active');

        }
        else if(targetElem.attr('for') === 'width'  ||
            targetElem.attr('for') === 'height'
        ){

            $(obj['one']).show();
            $(obj['marg_pad_pixel']).show();
            $(obj['step_section']).show();
            $(obj['value_type']).show();
            $(obj['marg_pad_auto']).show();

            $(obj['one']).addClass('ui-state-active');
            $(obj['marg_pad_pixel']).addClass('ui-state-active');


        }
        else if(targetElem.attr('for') === 'font-size'
        ){

            $(obj['one']).show();
            $(obj['marg_pad_pixel']).show();
            $(obj['step_section']).show();
            $(obj['value_type']).show();

            $(obj['one']).addClass('ui-state-active');
            $(obj['marg_pad_pixel']).addClass('ui-state-active');


        }else{
            // here switching based on button name + _group postfix

            $(obj[targetElem.attr('for') + '_group']).show();

        }



        // else if(targetElem.attr('for') === 'display'){
        //
        //     $(obj['display_group']).show();
        //
        //
        // }else if(targetElem.attr('for') === 'float'){
        //
        //     $(obj['float_group']).show();
        //
        //
        // }else if(targetElem.attr('for') === 'vertical-align'){
        //
        //     $(obj['vertical_align_group']).show();
        //
        //
        // }else if(targetElem.attr('for') === 'text-align'){
        //
        //     $(obj['text_align_group']).show();
        //
        //
        // }


    });
}










function contextMenuUIMainOld(){
    "use strict";

    $('label').on( "click", function(event) {


        var targetElem = $(event.target);


        if(targetElem.attr('for') === 'margin'
        ){
            $('.ui-state-active').removeClass('ui-state-active');

            $('.display_group').hide();
            $('.float_group').hide();
            $('.vertical-align_group').hide();
            $('.text-align_group').hide();

            $('#step_section').show();
            $('#value_type').show();
            $('.marg_pad').show();
            $("label[for='marg_pad_auto']").show();


            $("label[for='one']").addClass('ui-state-active');
            $("label[for='marg_pad_pixel']").addClass('ui-state-active');
            $("label[for='left']").addClass('ui-state-active');


        }else if(targetElem.attr('for') === 'padding'
        ){
            $('.ui-state-active').removeClass('ui-state-active');

            $('.display_group').hide();
            $('.float_group').hide();
            $('.vertical-align_group').hide();
            $('.text-align_group').hide();
            $("label[for='marg_pad_auto']").hide();

            $('#step_section').show();
            $('#value_type').show();
            $('.marg_pad').show();


            $("label[for='one']").addClass('ui-state-active');
            $("label[for='marg_pad_pixel']").addClass('ui-state-active');
            $("label[for='left']").addClass('ui-state-active');
        }
        else if(targetElem.attr('for') === 'width'  ||
            targetElem.attr('for') === 'height'
        ){
            $('.ui-state-active').removeClass('ui-state-active');

            $('.marg_pad').hide();
            $('.display_group').hide();
            $('.float_group').hide();
            $('.vertical-align_group').hide();
            $('.text-align_group').hide();

            $('#step_section').show();
            $('#value_type').show();
            $("label[for='marg_pad_auto']").show();
            $("label[for='one']").addClass('ui-state-active');
            $("label[for='marg_pad_pixel']").addClass('ui-state-active');


        }
        else if(targetElem.attr('for') === 'font-size'
        ){
            $('.ui-state-active').removeClass('ui-state-active');

            $('.marg_pad').hide();
            $('.display_group').hide();
            $('.float_group').hide();
            $('.vertical-align_group').hide();
            $("label[for='marg_pad_auto']").hide();
            $('.text-align_group').hide();

            $('#step_section').show();
            $('#value_type').show();
            $("label[for='one']").addClass('ui-state-active');
            $("label[for='marg_pad_pixel']").addClass('ui-state-active');


        }
        else if(targetElem.attr('for') === 'display'){
            $('.ui-state-active').removeClass('ui-state-active');

            $('#step_section').hide();
            $('#value_type').hide();
            $('.marg_pad').hide();

            $('.float_group').hide();
            $('.vertical-align_group').hide();
            $('.text-align_group').hide();
            $('.display_group').show();




        }else if(targetElem.attr('for') === 'float'){
            $('.ui-state-active').removeClass('ui-state-active');

            $('#step_section').hide();
            $('#value_type').hide();
            $('.marg_pad').hide();


            $('.display_group').hide();
            $('.float_group').show();
            $('.text-align_group').hide();
            $('.vertical-align_group').hide();

        }else if(targetElem.attr('for') === 'vertical-align'){
            $('.ui-state-active').removeClass('ui-state-active');

            $('#step_section').hide();
            $('#value_type').hide();
            $('.marg_pad').hide();


            $('.display_group').hide();
            $('.float_group').hide();
            $('.text-align_group').hide();
            $('.vertical-align_group').show();

        }else if(targetElem.attr('for') === 'text-align'){
            $('.ui-state-active').removeClass('ui-state-active');

            $('#step_section').hide();
            $('#value_type').hide();
            $('.marg_pad').hide();
            $('.vertical-align_group').hide();

            $('.display_group').hide();
            $('.float_group').hide();
            $('.text-align_group').show();

        }


    });
}





function getLabelFromInput(inputSelector){
    "use strict";

    return $('label[for="'+ inputSelector.attr('id') +'"]');

}