
mainFunc();

// vars

var dataHtml = '';


function mainFunc() {


    $(document).mousedown(function(){

        // checking modes
        if(mode !== 'template_edit' || $('.editor_activeTemplate').length === 0){
            return;
        }


        targetElem = $(event.target);

        if(targetElem.hasClass( "editor_itemLabel" ) || targetElem.attr('id') == 'editor_clone'){

            $('#editor_clone').remove();

            templateName = targetElem.text();


            $("body").append(
                '<div id="editor_clone"></div>'
            );



            $(document).mousemove(function(event){



                moveAt($('#editor_clone'), event);

                currElem = getFullElemName(event.target);

                $('#editor_clone').text(currElem);
                $('#editor_clone').css("cursor", "default");


            });




            $(document).mouseup(function(event){

                targetElem = $(event.target);


                if(targetElem.hasClass( "editor_itemLabel" ))
                {
                    $(document).unbind('mousemove');
                    $('#editor_clone').remove();
                    $(document).unbind('mouseup');
                    return;
                }



                if(targetElem.attr('id') === 'editor_page-area'){
                    // console.log("*** inside editor_page-area ***");
                    getTemplateCode(event);

                }else{

                    $.each(targetElem.parents(), function (index, value) {

                        if($(this).attr('id') === 'editor_page-area'){

                            // console.log("*** inside editor_page-area ***");
                            getTemplateCode(event);

                        }
                    });

                }



                $(targetElem).html($(targetElem).children());


                updateHtmlEditor();
                $(document).unbind('mousemove');
                $('#editor_clone').remove();
                $(document).unbind('mouseup');

            });

        }


    });


}


function getTemplateCode(event) {

    $.ajax({
        type: 'POST',
        url: '/sendTemplateName',
        data: {
            'templateName': templateName,
            'templateTab':template_tab
        },
        success: function(data) {

            dataHtml = data.htmlVal;

            targetElem = $(event.target);
            targetElem.append(data.htmlVal);

            var htmlVal = $("#editor_page-area").html();
            htmlVal = removeStyleTag(htmlVal);
            htmlVal = beautifyString(htmlVal);
            $('#editor_htmlCode').val(htmlVal);

            var cssVal = data.cssVal;

            if($('#editor_cssCode').val().length === 0 || $('#editor_cssCode').val() === ' '){

                $('#editor_cssCode').val(cssVal);

            }else{

                cssVal = data.cssVal + $('#editor_cssCode').val(); // merge old and new css
                cssVal = mergeSelectorsMainFunction(cssVal); // merging selectors
                $('#editor_cssCode').val(cssVal);
            }

            UpdateHtmlCssChanges(htmlVal, cssVal);
            applyTempStyle(data.htmlVal, event);
            refreshHierarchy();

        }
    });
}


function applyTempStyle(dataHtml, event){

    var inLineStyle = '';

    targetElem = $(event.target);
    var kids = targetElem.children();

    for (var i = 0; i < kids.length; i++) {

        if($(kids[i]).prop('tagName') === 'STYLE'){
            kids.splice(i, 1);
            i--;
        }

    }

    var actualElem = kids[kids.length - 1];

    if(dataHtml.indexOf('default_container') !== -1 ||
        dataHtml.indexOf('default_row') !== -1 ||
        dataHtml.indexOf('col3') !== -1
    ){

        inLineStyle =
            "text-align: center;" +
            "font-size:50px;" +
            "color:#a5a5a5;" +
            "font-weight:800;" +
            "background-color: #c2dce0;" +
            "padding: 10px;" +
            "border: 1px solid black;";


        $(actualElem).attr('style', inLineStyle);

    }

    updateHtmlEditor();
}







function moveAt(elem, event) {

    elem.css({

        'left':(event.pageX - 50) + 'px',
        'top':(event.pageY - 35) + 'px'

    });
}




