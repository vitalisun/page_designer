
mainCssEdit();
removeClass();

function mainCssEdit() {


    // declaring vars
    var gridColType = '';
    var gridColVal = 0;
    var toApplyVal = 0;



    $('.grid_col').on('mousedown', function (event) {

        $('.ui-state-active').removeClass('ui-state-active');

        gridValue = $(this).text();
    });




    $(document).on('mousedown', function (event) {

        ///////////////////////////////////////////////////////////////
        // checks
        if(
            validateClickArea('editor_left-panel', event.target) ||
            validateClickArea('editor_right-panel', event.target) ||
            validateClickArea('editor_editor-pane', event.target) ||
            mode !== 'grid_edit'
        ){
            return;
        }



        ///////////////////////////////////////////////////////////////

        var gridColLabels = $('.grid_col');
        for (i = 0; i < gridColLabels.length; i++) {
            if($(gridColLabels[i]).hasClass('ui-state-active')){

                labelBtn = $(gridColLabels[i]);
                gridColType = $(gridColLabels[i]).attr('for');
                break;
            }

        }


        ///////////////////////////////////////////////////////////////

        startX = event.pageX;

        ///////////////////////////////////////////////////////////////

        var classOfEl = $(selectedTarget).attr('class');

        if(classOfEl === undefined){
            return;
        }

        classOfEl = classOfEl.replace('highlighted', '');

        var colInfoArr = getGridInfo(classOfEl);

        if(colInfoArr.length > 0){
            for (var i = 0; i < colInfoArr.length; i++) {
                if(colInfoArr[i].screenSize === gridColType)
                {
                    labelBtn.text('col-' + colInfoArr[i].screenSize + '-' + value_limit(colInfoArr[i].colWidth, 1, 12));
                    gridColVal = colInfoArr[i].colWidth;
                    break;
                }
            }
        }






        ///////////////////////////////////////////////////////////////


        $(document).mousemove(function(event){


            var currX = event.pageX;

            toApplyVal = applyChangeColVal(gridColVal, currX);

            var classOfElArr = $(selectedTarget).attr('class').split(' ');
            for (i = 0; i < classOfElArr.length; i++) {



                if(classOfElArr[i].indexOf(gridColType) !== -1){


                    // checking fitness
                    var gridColTypeLength = gridColType.split('-').length; // 1 (width) or 2 (offset)
                    var actualClassLength = classOfElArr[i].split('-').length; // 3 (width) or 4 (offset)

                    // width
                    if(gridColTypeLength === 1 && actualClassLength === 3){

                        $(selectedTarget).removeClass(classOfElArr[i]);
                        break;

                        // offset
                    }else if(gridColTypeLength === 2 && actualClassLength === 4){

                        $(selectedTarget).removeClass(classOfElArr[i]);
                        break;
                    }
                }
            }

            var classToAdd = 'col-' + gridColType + '-' + value_limit(toApplyVal, 1, 12);
            $(selectedTarget).addClass(classToAdd);

            gridValue = classToAdd;
            labelBtn.text(classToAdd);


            // updating element name display
            updateElementName();

        });




        $(document).mouseup(function(event){

            $(document).unbind('mousemove');
            $(document).unbind('mouseup');

        });

    });


}


function applyChangeColVal(gridColVal, currX){

    var shift = roundToStep(currX - startX, 50)/50;
    var finalVal = parseInt(gridColVal) + shift;

    return finalVal;
}






function getGridInfo(classesString){

    var arr = classesString.split(' ');

    var dataArr = [];

    for (i = 0; i < arr.length; i++) {
        if(arr[i].indexOf('col-') !== -1){

            var parts = arr[i].split('-');


            // width
            if(parts.length === 3){

                var obj = {
                    screenSize: parts[1],
                    colWidth: parts[2]
                };

                // offset
            }else if(parts.length === 4){


                var obj = {
                    screenSize: parts[1] + '-' + parts[2],
                    colWidth: parts[3]
                };

            }


            dataArr.push(obj);
        }
    }



    return dataArr;
}


function removeClass(){

    $('.gridClassRemove').on('mousedown', function (event) {


        var classOfEl = $(selectedTarget).attr('class');

        var arr = classOfEl.split(' ');

        for (i = 0; i < arr.length; i++) {
            if (arr[i].indexOf('col') !== -1) {

                $(selectedTarget).removeClass(arr[i]);

            }
        }

        // reset values
        var gridColLabels = $('.grid_col');
        for (i = 0; i < gridColLabels.length; i++) {
            labelBtn = $(gridColLabels[i]);
            var actualClass = labelBtn.text();
            var lastInd = actualClass.lastIndexOf('-');
            actualClass = actualClass.slice(0, lastInd) + '-' + '1';
            labelBtn.text(actualClass);
        }


    });


}



