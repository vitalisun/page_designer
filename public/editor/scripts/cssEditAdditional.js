
cssEditAdditionalMain();




function cssEditAdditionalMain(){


    getToolName();
    removeRule();
    zeroRule();
}




function getToolName(){

    $('.cssAdditional').on('click', function (event) {


        function getIt() {

            var currTool = $('.tool_btn.ui-state-active').text();


            if($(event.target).parent().hasClass(currTool + '_group')){
                currTool = currTool.replace('_', '-');
                var rule = currTool + ':' + $(event.target).attr('id');
                insertRule(rule);

            }

        }

        window.setTimeout( getIt, 100 );

    });


}







function insertRule(rule){
    
    if(cssSelector === ''){
        console.log("-- no selector --");
        return;
    }

    cssDB = parseCSSInput($('#editor_cssCode').val());

    var stylesToCheck = [];
    stylesToCheck.push(rule);

    mergeStyle(cssDB, stylesToCheck, rule);

    var updatedCss = assembleRulesOutput(cssDB);

    $('#editor_cssCode').val(updatedCss);
    UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());


}







function removeRule(){

    $('.removeRule').on('click', function (event) {
        
        if(cssSelector === ''){
            console.log("-- no selector --");
            return;
        }


        console.log("cssSelector -- " + cssSelector);

        cssDB = parseCSSInput($('#editor_cssCode').val());
        var spliced = false;


        for (i = 0; i < cssDB.length; i++) {

            console.log("cssDB[i].selectorKey  -- " + cssDB[i].selectorKey);
            console.log("$('.highest_priority').text() -- " + $('.highest_priority').text());
            console.log($('.highest_priority2'));



            if(cssDB[i].selectorKey === $('.highest_priority').text() ||
                cssDB[i].selectorKey === $('.highest_priority2').text()
            ){

                var props = cssDB[i].propertiesKey;


                for (b = 0; b < props.length; b++) {

                    var rule = props[b].split(':')[0];
                    console.log("rule -- " + rule);
                    if(rule === $('#editedRule').text()){

                        props.splice(b, 1);
                        spliced = true;
                        break;
                    }else if(rule === $('#editedRule').text().split('-')[0] &&
                        $('#editedRule').text().split('-')[1] === 'all'
                    ){

                        props.splice(b, 1);
                        spliced = true;
                        break;

                    }

                }

            }

            if(spliced){
                break;
            }

        }


        var updatedCss = assembleRulesOutput(cssDB);

        $('#editor_cssCode').val(updatedCss);
        UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());


    });

}



function zeroRule(){


    $('.zeroRule').on('click', function (event) {

 
        if(cssSelector === ''){
            console.log("-- no selector --");
            return;
        }




        cssDB = parseCSSInput($('#editor_cssCode').val());
        var zeroed = false;


        for (i = 0; i < cssDB.length; i++) {


            if(cssDB[i].selectorKey === $('.highest_priority').text()){

                var props = cssDB[i].propertiesKey;

                for (b = 0; b < props.length; b++) {

                    var rule = props[b].split(':')[0];

                    if(rule === $('#editedRule').text()){

                        props[b] = rule + ': 0';
                        console.log("props[b] -- " + props[b]);
                        cssDB[i].propertiesKey = props;
                        zeroed = true;
                        break;
                    }else if(rule === $('#editedRule').text().split('-')[0] &&
                        $('#editedRule').text().split('-')[1] === 'all'
                    ){

                        props[b] = rule + ': 0';
                        console.log("props[b] -- " + props[b]);
                        cssDB[i].propertiesKey = props;
                        zeroed = true;
                        break;

                    }

                }

            }

            if(zeroed){
                break;
            }

        }


        var updatedCss = assembleRulesOutput(cssDB);

        $('#editor_cssCode').val(updatedCss);
        UpdateHtmlCssChanges($('#editor_htmlCode').val(), $('#editor_cssCode').val());


    });


}