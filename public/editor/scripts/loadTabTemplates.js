$(document).ready(function() {
    loadTabTemplatesMain();
});

function loadTabTemplatesMain(){

    $('#editor_presetList1').empty();
    $('#editor_presetList2').empty();
    $('#editor_presetList3').empty();
    
    var tabs = ["#editor_presetList1", "#editor_presetList2", "#editor_presetList3"];
    
    for (var a = 0; a < tabs.length; a++) {
        
        ajaxRequestGetTemplates(tabs[a]);
        
    }
    
}





function ajaxRequestGetTemplates(tabId){

    $.ajax({
        type: 'POST',
        url: '/getTemplatesForTabs',
        data: {
            'templateTabId': tabId
        },
        success: function(data) {
            
            for (var i = 0; i < data.length; i++) {
                
                var item = data[i];
                
                if (item.split('.')[1] == 'html') {
   
                    itemName = item.split('.')[0];

                    var saveImage = '<img src="save.png" class="editor_saveImage"/>';
                    var delImage = '<img src="delete.png" class="editor_delImage"/>';

                    var newElem = '<li class="editor_item list-group-item">' +
                        '<div class="editor_itemLabel">' + itemName + '</div>' +
                        saveImage +
                        delImage +
                        '</li>';
                    
                    $(tabId).append(newElem);
                }
            }
        }
    });


}