
// prevent selection in right part
$(document).ready(function(){
    $("body").css("-webkit-user-select","none");
    $("body").css("-moz-user-select","none");
    $("body").css("-ms-user-select","none");
    $("body").css("-o-user-select","none");
    $("body").css("user-select","none");


    ClickFunc();
    SavePresetFunc();
    SelectTemplate();
    ActiveTemplateFunc();
    changeTemplateTab();


});

function ClickFunc(){

    $(document).click(function(event) {

        targetElem = $(event.target);
        destination = targetElem.offset();


        // clearing temporary elements
        if(!targetElem.hasClass( "editor_protected" )){
            $("#editor_loadWindow").remove();
        }


        if(targetElem.hasClass( "editor_itemLabel" )){

            createInputTemplateName(targetElem);

        }

        else if(targetElem.hasClass( "editor_save" )) {

            $(".editor_textBox").remove();
            // console.log(targetElem.attr('class'));
            typeOfInput = 'add';

            var newElem = '<input type="text" value="new_preset" class="editor_textBox"></input>';

            var actualTab = getTabId(template_tab);
            $( '#' + actualTab).append(newElem);

            $(".editor_textBox").focus().select();
        }

        else if(targetElem.hasClass( "editor_load" )){

            LoadTemplateFunc();
        }

        else if(targetElem.hasClass( "editor_textBox" )){
            // console.log(targetElem);
            return;

        }
        else if(targetElem.hasClass( "editor_delImage" )){
            targetElem.parent().remove();
            return;

        }
        else if(targetElem.hasClass( "editor_saveImage" )){

            // sending html via ajax to server
            $('.highlighted').removeAttr("style").removeClass('highlighted');
            isHighlighted = false;
            var htmlVal = $("#editor_page-area").html();
            htmlVal = updateHtmlEditor(htmlVal);
            var cssVal = $('#editor_cssCode').val();
            itemName = targetElem.siblings(".editor_itemLabel").text();
            FromClientToServer(htmlVal, cssVal, itemName);

            $(targetElem).addClass('editor_saveImage_clicked');
            setTimeout(function() {
                $(targetElem).removeClass('editor_saveImage_clicked');
            }, 200);

            return;

        }else{

            // console.log(targetElem);
            $(".editor_textBox").remove();
            return;
        }

    });




}



function createInputTemplateName(targetElem){

    currItem = targetElem;

    // clearing temporary elements
    $(".editor_textBox").remove();

    typeOfInput = 'edit';

    var inputEl = $('<input type="text" class="editor_textBox"></input>');

    var actualTab = getTabId(template_tab);

    $('#'+actualTab).append(inputEl);

    $(inputEl).val(targetElem.text());
    $(inputEl).css({
        position:"absolute",
        top: destination.top
    });

    $(".editor_textBox").focus().select();

}



function LoadTemplateFunc(){

    $("#editor_loadWindow").remove();

    $.ajax({
        type: 'POST',
        url: '/loadTemplate',
        success: function(data) {


            $("body").append(
                '<div id="editor_loadWindow" class="ui-widget-content editor_protected">' +
                '<p class="editor_protected">templates</p>' +
                '<div class="editor_protected" id="editor_loadWindowInnerPart">' +
                '<ul class="editor_protected">' +
                '</ul>' +
                '</div>' +
                '</div>'
            );

            data.forEach(function(item, data) {
                $("#editor_loadWindowInnerPart ul").append('<li class="editor_protected listItem">'+
                    item.split('.')[0] +
                    '</li>');
            });

            $( "#editor_loadWindow" ).draggable();

        }

    });

}



function SavePresetFunc(){




    // save preset name
    $(document).keypress(function(event){

        itemName = $(".editor_textBox").val();
        if(itemName === undefined)
            return;

        var actualTab = getTabId(template_tab);

        var keycode = (event.keyCode ? event.keyCode : event.which);
        
        if(keycode == '13'){



            var saveImage = '<img src="save.png" class="editor_saveImage"/>';
            var delImage = '<img src="delete.png" class="editor_delImage"/>';
            // console.log(itemName);
            // replacing spaces with underscore
            itemName = itemName.replace(/ /g, "_");

            var newElem = '<li class="editor_item list-group-item">' +
                '<div class="editor_itemLabel">'+ itemName + '</div>' +
                saveImage +
                delImage +
                '</li>';


            switch (typeOfInput) {

                case 'add':
                    $('#' + actualTab).append(newElem);
                    FromClientToServer(" ", " ", itemName);

                    $(".editor_textBox").remove();
                    break;

                case 'edit':

                    currItem.html(itemName);
                    $(".editor_textBox").remove();
                    break;

            }

        }
        event.stopPropagation();
    });

}


function FromClientToServer(textVal, cssVal, itemName){


    // console.log(itemName);

    $.ajax({
        type: 'POST',
        url: '/saveToFile',
        data: {
            'text': textVal,
            'css': cssVal,
            'templateName':itemName,
            'templateTab':template_tab
        }
    });

}

function SelectTemplate(){


    $("body").on("click", ".listItem", function(){

        itemName = $(this).text();
        //console.log(itemName);

        var saveImage = '<img src="save.png" class="editor_saveImage"/>';
        var delImage = '<img src="delete.png" class="editor_delImage"/>';

        var newElem = '<li class="editor_item list-group-item">' +
            '<div class="editor_itemLabel">'+ itemName + '</div>' +
            saveImage +
            delImage +
            '</li>';

        var actualTab = getTabId(template_tab);
        $( '#' + actualTab).append(newElem);


    });

}
// proccesing active template
function ActiveTemplateFunc(){

    var hasActiveState = false;
    var currActiveIndex = 0;

    $(document).keydown(function(event) {

        var actualTab = getTabId(template_tab);

        if(actualTab === 'editor_presetList1' || actualTab === 'editor_presetList2'){

            return;
        }

        var allItems = $('#' + actualTab).children();
        if (allItems.length > 0) {
            hasActiveState = true;
        }

        if(hasActiveState && !isHighlighted){

            switch (event.keyCode) {
                case 38:
                    // console.log("up key pressed");

                    if(currActiveIndex > 0){
                        currActiveIndex--;
                    }
                    $(allItems[currActiveIndex+1]).removeClass("editor_activeTemplate");
                    $(allItems[currActiveIndex]).addClass("editor_activeTemplate");
                    FeedTemplate();

                    break;
                case 40:
                    // console.log("down key pressed");

                    if(currActiveIndex < allItems.length-1){
                        currActiveIndex++;
                    }
                    $(allItems[currActiveIndex-1]).removeClass("editor_activeTemplate");
                    $(allItems[currActiveIndex]).addClass("editor_activeTemplate");
                    FeedTemplate();

                    break;
            }



        }

    });

}






function FeedTemplate(){

    var templName = $('.editor_activeTemplate').text();
    var linkNum = 0;

    // console.log("templName -- " + templName);

    if( $('.editor_activeTemplate').length )
    {
        // console.log($('.editor_activeTemplate').text());

        $.ajax({
            type: 'POST',
            url: '/sendTemplateName',
            data: {
                'templateName': templName,
                'templateTab':template_tab
            },
            success: function(data) {



                if(data.htmlVal != ''){
                    $('#editor_htmlCode').val(data.htmlVal);
                }


                if(data.cssVal != ''){
                    $('#editor_cssCode').val(data.cssVal);

                    // here checking if a link already exist
                    var allLinks = $( "head" ).find( "link" );
                    
                    $(allLinks).each(function() {
                        var actualLink = $(this).attr('href');

                        if(actualLink.indexOf(templName) !== -1)
                        {
                            linkNum ++;
                        }

                    });

                    if(linkNum === 0){
                        $('head').append(data.cssLink);
                    }

                }

                
                htmlVal = $('#editor_htmlCode').val();
                cssVal = $('#editor_cssCode').val();
                UpdateHtmlCssChanges(htmlVal, cssVal);
                hierarchyMain();
            }

        });
    }

}


function changeTemplateTab(){

    $('.template_basic').on('click', function(){
        template_tab = 'basic';
        // loadBootstrapBasicMain();
    });

    $('.template_more').on('click', function(){
        template_tab = 'more';
        // loadBootstrapBasicMain();
    });

    $('.template_custom').on('click', function(){
        template_tab = 'custom';
        // loadBootstrapBasicMain();
    });


}


function getTabId(tab){

    var actualTab;

    if(tab === 'basic'){
        actualTab = 'editor_presetList1';
    }else if(tab === 'more'){
        actualTab = 'editor_presetList2';
    }else if(tab === 'custom'){
        actualTab = 'editor_presetList3';
    }

    return actualTab;
}
