

// executes from FeedTemplate() of saveLoadPreset.js
function hierarchyMain() {


    $('.chierarchyElemExpander').unbind('on');



    assignTechClasses();
    buildHierarchy();
    processHierarchyVisibility();
    toggleVisibility();
    selectElement();
    // highlightHierarchyElementOnSelection();

}











function assignTechClasses(){





    fullHierarchyArr = $('#editor_page-area').find('*');



    for(var i = 0; i < fullHierarchyArr.length; i++){

        $(fullHierarchyArr[i]).removeClass('closed display_off outer_display_off');
        var techID = getTechIdFromClass($(fullHierarchyArr[i]).attr('class'));
        $(fullHierarchyArr[i]).removeClass(techID);
    }





    // assign id
    for(var i = 0; i < fullHierarchyArr.length; i++){

        // excluding style tag processing
        if($(fullHierarchyArr[i]).prop('tagName').toLowerCase() === 'style'){
            continue;
        }

        $(fullHierarchyArr[i]).addClass('techID_' + i);


    }


    var kidsArr = $('#editor_page-area').children();

    for(var i = 0; i < kidsArr.length; i++){


        // excluding style tag processing
        if($(kidsArr[i]).prop('tagName').toLowerCase() === 'style'){
            continue;
        }


        $(kidsArr[i]).addClass('closed');

        var downHierarchy = $(kidsArr[i]).find('*');
        for(var a = 0; a < downHierarchy.length; a++){

            $(downHierarchy[a]).addClass('closed display_off');

        }

    }


}













function buildHierarchy(){


    $('#hierarchy' ).empty();


    for(var i = 0; i < fullHierarchyArr.length; i++){

        // excluding style tag processing
        if($(fullHierarchyArr[i]).prop('tagName').toLowerCase() === 'style'){
            continue;
        }


        var elemStr = '<div class="chierarchyElem">'+
            '<div class="chierarchyElemExpander">'+
            '</div>' +
            '<div class="chierarchyElemLabel">'+
            getFullElemNameFromSelector($(fullHierarchyArr[i])) +
            '</div>' +
            '</div>';


        var elem = $(elemStr).appendTo($('#hierarchy'));


        var techID = getTechIdFromClass($(fullHierarchyArr[i]).attr('class'));


        $(elem).addClass('H_' + techID);
        $(elem).css('left', 0 + 'px');


    }


    setOffSets();

}












function setOffSets(){


    for(var i = 0; i < fullHierarchyArr.length; i++){


        var parArr = $(fullHierarchyArr[i]).parents();
        var tabOffset = (parArr.length - 6)*20;
        var techID = getTechIdFromClass($(fullHierarchyArr[i]).attr('class'));

        $('.H_' + techID).css('left', tabOffset + 'px');

    }

}














function processHierarchyVisibility(){


    var hierarchyArr = $('#hierarchy').children();

    for(var i = 0; i < hierarchyArr.length; i++){

        var idValHierarchy = getTechIdFromClass($(hierarchyArr[i]).attr('class'));
        var idValElem = idValHierarchy.replace('H_', '');
        var elem = $('.' + idValElem);
        var hierarchyElem = $(hierarchyArr[i]);


        if($(elem).hasClass('display_off') || $(elem).hasClass('outer_display_off')){

            $(hierarchyElem).hide();

        }else{

            $(hierarchyElem).show();

        }
    }


}
















function toggleVisibility(){

    $('.chierarchyElemExpander').on('mousedown', function(event) {


        var idValHierarchy = getTechIdFromClass($(event.target).parent().attr('class'));
        var idValElem = idValHierarchy.replace('H_', '');
        var elem = $('.' + idValElem);
        var altKeypress = false;


        if(event.altKey){


            $(elem).toggleClass('closed');
            altKeypress = true;
            processClosedClassInfluense($(elem), altKeypress);

        }else{

            $(elem).toggleClass('closed');
            altKeypress = false;
            processClosedClassInfluense($(elem), altKeypress);

        }





    });



}
















function processClosedClassInfluense(elem, altKeypress){


    var elemKidsArr = $(elem).children();
    elemDownHierarchyArr = $(elem).find("*");
    var elemExpander = getExpanderElem(elem);


    if(!altKeypress){

        if ($(elem).hasClass('closed')) {

            $(elemExpander).removeClass('chierarchyElemExpanderOn');

            for(var i = 0; i < elemDownHierarchyArr.length; i++){
                $(elemDownHierarchyArr[i]).addClass('outer_display_off');

            }

            for(var i = 0; i < elemKidsArr.length; i++){
                $(elemKidsArr[i]).addClass('display_off');
            }

        }else{

            $(elemExpander).addClass('chierarchyElemExpanderOn');

            for(var i = 0; i < elemDownHierarchyArr.length; i++){

                if(!checkClosedParent(elemDownHierarchyArr[i])){
                    $(elemDownHierarchyArr[i]).removeClass('outer_display_off');
                }


            }

            for(var i = 0; i < elemKidsArr.length; i++){
                $(elemKidsArr[i]).removeClass('display_off');
            }
        }

        processHierarchyVisibility();

    } else if(altKeypress){


        if ($(elem).hasClass('closed')) {


            $(elemExpander).removeClass('chierarchyElemExpanderOn');

            for (var i = 0; i < elemDownHierarchyArr.length; i++) {

                $(elemDownHierarchyArr[i]).addClass('closed display_off outer_display_off');

                var elemExpander2 = getExpanderElem(elemDownHierarchyArr[i]);
                $(elemExpander2).removeClass('chierarchyElemExpanderOn');

            }

        }else{

            $(elemExpander).addClass('chierarchyElemExpanderOn');


            for (var i = 0; i < elemDownHierarchyArr.length; i++) {

                $(elemDownHierarchyArr[i]).removeClass('closed display_off outer_display_off');

                var elemExpander2 = getExpanderElem(elemDownHierarchyArr[i]);
                $(elemExpander2).addClass('chierarchyElemExpanderOn');

            }

        }


        processHierarchyVisibility();

    }


    // checking if any of ancestors has 'closed' class
    function checkClosedParent(checkedElem){

        var elemPars = $(checkedElem).parents();

        for (var a = 0; a < elemPars.length; a++) {

            if ($(elemPars[a]).hasClass('closed')) {

                return true;

            }

        }


        return false;

    }


}













function getExpanderElem(elem){

    var idValHierarchy = getTechIdFromClass($(elem).attr('class'));
    var elemHierarchy = $('.H_' + idValHierarchy);

    var elemExpander = $(elemHierarchy.children()[0]);

    return elemExpander;

}














function getTechIdFromClass(classStr){


    if(classStr === undefined)
        return;

// getting tech id
    var classes = classStr.split(' ');
    var techID = '';


    for(var a = 0; a < classes.length; a++){

        if(classes[a].indexOf('techID_') !== -1){

            techID = classes[a];

        }

    }

    return techID;

}










function selectElement() {


    $('#hierarchy').on('mousedown', '.chierarchyElem', function(event) {


        if(!$(event.target).hasClass('chierarchyElemLabel'))
            return;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // processing highlighting hierarchy element

        var chSelected = $('.chierarchyElemSelected');
        for(var a = 0; a < chSelected.length; a++){

            if($(chSelected[a]).parent().attr('class') === $(event.target).parent().attr('class')){
                continue;
            }else{

                $(chSelected[a]).removeClass('chierarchyElemSelected');
            }
        }


        $(event.target).toggleClass('chierarchyElemSelected');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        var idValHierarchy = getTechIdFromClass($(event.target).parent().attr('class'));
        var idValElem = idValHierarchy.replace('H_', '');
        var elem = $('.' + idValElem);

        if($(event.target).hasClass('chierarchyElemSelected')){

            selectedTarget = elem;
            onClickHighlight(elem);

        }else{

            clearSelection();

        }

        event.stopImmediatePropagation();
    });


}











function removeHierarchyElement(elem) {

    var elemArr = $(elem).find("*");
    var idVal = getTechIdFromClass($(elem).attr('class'));



    for(var a = 0; a < elemArr.length; a++){

        var idValItem = getTechIdFromClass($(elemArr[a]).attr('class'));
        $('.H_' + idValItem).remove();

    }



    $('.H_' + idVal).remove();



}










function refreshHierarchy(){


    var idArr = saveHierarchyState();
    hierarchyMain();
    restoreHierarchyState(idArr);


}













function saveHierarchyState() {

    var hierarchyArr = $('#hierarchy').children();

    var openedIdArr = [];


    for(var a = 0; a < hierarchyArr.length; a++){

        var expElem = $(hierarchyArr[a]).children()[0];

        if($(expElem).hasClass('chierarchyElemExpanderOn')){

            var openedId = getTechIdFromClass($(hierarchyArr[a]).attr('class'));
            openedIdArr.push(openedId);

        }

    }


    return openedIdArr;

}











// now function works only for ancestors
function restoreHierarchyState(idArr) {

    var hierarchyArr = $('#hierarchy').children();


    for(var a = 0; a < hierarchyArr.length; a++){

        var idVal = getTechIdFromClass($(hierarchyArr[a]).attr('class'));

        for(var b = 0; b < idArr.length; b++){

            if(idVal === idArr[b]){

                var expElem = $(hierarchyArr[a]).children()[0];
                var elem = $('.' + idVal.replace('H_', ''));

                $(expElem).addClass('chierarchyElemExpanderOn');
                $(elem).removeClass('closed');

                processClosedClassInfluense($(elem), false);
            }
        }

    }




}










function highlightHierarchyElementOnSelection(){

    // selectionChangedEvent

    $(document).on('selectionChangedEvent', function(event) {


        var idVal = getTechIdFromClass($('.highlighted').attr('class'));

        if(idVal === undefined)
            return;

        var hierarchyElem = $('.H_' + idVal);

        $(hierarchyElem).siblings().removeClass('chierarchyElemSelected');
        $(hierarchyElem).addClass('chierarchyElemSelected');

    });


}

