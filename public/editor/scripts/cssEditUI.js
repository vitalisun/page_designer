// this script process behaviour of tool panel ui



mainFunc();



function mainFunc(){
    "use strict";

    $(document).on( "click", function(event) {

        var targetElem = $(event.target);

        var arr;


        if(targetElem.attr('name') === 'margin-radio'){
            arr = ['padding-radio', 'width-radio', 'text-radio'];
            uiProcess(targetElem, arr);
        }
        else if(targetElem.attr('name') === 'padding-radio'){
            arr = ['margin-radio', 'width-radio', 'text-radio'];
            uiProcess(targetElem, arr);
        }
        else if(targetElem.attr('name') === 'width-radio'){
            arr = ['padding-radio', 'margin-radio', 'text-radio'];
            uiProcess(targetElem, arr);
        }
        else if(targetElem.attr('name') === 'text-radio'){
            arr = ['padding-radio', 'width-radio', 'margin-radio', 'value_type'];
            uiProcess(targetElem, arr);
        }

    });
}





function uiProcess(clickedEl, arr){
    "use strict";


    var label = getLabelFromInput(clickedEl);
    label.addClass('ui-checkboxradio-checked ui-state-active');


    $.each(arr, function (index, value) {
        if (arr[index] !== undefined) {
            $.each($('input[name=' + arr[index] + ']'), function () {

                label = $('label[for="' + $(this).attr('id') + '"]');
                label.removeClass('ui-checkboxradio-checked ui-state-active');
            });
        }
    });


}




function getLabelFromInput(inputSelector){
    "use strict";

    return $('label[for="'+ inputSelector.attr('id') +'"]');

}