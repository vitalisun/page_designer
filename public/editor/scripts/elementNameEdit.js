elementNameEditMain();


function elementNameEditMain(){

    changeNameUI();
    changeName();
}


function changeNameUI() {


    $(document).on("contextmenu",  function(event) {

        // just preventing on the section

        var hasPar = hasParent(event.target, 'elementNameDiv');
        if (hasPar) {

            event.preventDefault();

        }

    });


    $(document).on("mousedown", function (event) {
        // $('.context_new').on('mousedown', function () {

        if($(selectedTarget).prop('tagName') === undefined){
            return;
        }

        var hasPar = hasParent(event.target, 'elementNameDiv');

        if(hasPar && event.which === 1){
            event.preventDefault();
            $('#editElementNameWindow').show(100);
            $("#editTag").val($(selectedTarget).prop('tagName').toLowerCase());
            $("#editId").val($(selectedTarget).attr('id'));





            classSelectedElemTech = techClassesString($(selectedTarget).attr('class'));
            classSelectedElemNoTech = noTechClassesString($(selectedTarget).attr('class'));
            // console.log("classSelectedElemTech -- " + classSelectedElemTech);
            // console.log("classSelectedElemNoTech -- " + classSelectedElemNoTech);
            $("#editClass").val($.trim(classSelectedElemNoTech));

        }


    });

    $(document).on("mousedown", function (event) {


        var hasPar = hasParent(event.target, 'editElementNameWindow');
        var hasPar2 = hasParent(event.target, 'elementNameDiv');
        if(!hasPar && !hasPar2){
            $('#editElementNameWindow').hide(100);
        }else if(hasPar && $(event.target).hasClass('btn')){

            $('#editElementNameWindow').hide(100);
        }

    });




    $(document).on('keypress', function(event) {


        if(event.keyCode == '13' && $('#editElementNameWindow').css('display') !== 'none'){

            $('#editElementNameWindow').hide(100);

        }

    });


}








function changeName(){


    $("#editTag").keyup(function(){

        editTagName();

    });


    $("#editId").keyup(function(){

        editIdName();

    });


    $("#editClass").keyup(function(){

        editClassName();

    });



}



function editTagName(){

    var newTag = $("#editTag").val();
    if(newTag === ''){
        newTag = 'div';
        $("#editTag").val('div');
    }
    selectedTarget = $(selectedTarget).tagName(newTag);
    updateHtmlEditor();
    updateElementName();

}

function editIdName(){

    $(selectedTarget).attr("id", $("#editId").val());
    updateElementName();

}


function editClassName(){


    $(selectedTarget).attr("class", $("#editClass").val() + classSelectedElemTech);
    updateElementName();

}