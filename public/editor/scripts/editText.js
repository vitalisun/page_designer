
editTextMain();


function editTextMain() {


// removing textfield
    $(document).on('mousedown', function(event) {

        if($(event.target).prop('tagName').toLowerCase() !== 'input'){
            // console.log("check..");
            $('#textEditInput').remove();
        }

    });


// clicking on element to edit text
    $(document).on('mousedown', function(event) {

        if($(event.target).attr('id') === 'textEditInput' ||
            !event.ctrlKey
        ){
            return;
        }




        editTextTarget = event.target;

        var inputEl = "<input type='text' autofocus value=' ' id='textEditInput'></input>";
        destination = $(editTextTarget).offset();
        var inLineStyle =
            "position: absolute; " +
            "top:" + destination.top + 'px; ' +
            "width:" + $(editTextTarget).width() + 'px; ' +
            "height:" + $(editTextTarget).height() + 'px; ' +
            "left:" + destination.left + 'px';



        $('#editor_page-area').append(inputEl);
        $('#textEditInput').attr('style', inLineStyle);

        setTimeout("$('#textEditInput').focus()", 10);


    });


// saving text
    $(document).on('keypress', function(event) {

        if(event.keyCode == '13'){

            $(editTextTarget).text($('#textEditInput').val());
            $('#textEditInput').remove();

        }

    });



}

