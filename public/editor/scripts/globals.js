// globals
var isHighlighted = false;
var gridEdit_mode = false;
var mode = 'template_edit';
var selectedTarget = '';
var actualTarget = '';
var editTextTarget = '';
var template_tab = 'custom';
var cssSelector = '';
var classSelectedElemNoTech = '';
var classSelectedElemTech = '';

// defining globals
var itemName = '';
var targetElem;
var currItem;
var destination;
var typeOfInput = '';


var currElem = '';
var templateName = '';
var customError = '';


var startX;
var cssRule;    // string like 'font-size'
var prevCssRule = '';
var currCssVal; // string like '24px'
var prevCssValFull = '';
var currEditedRule = '';
var prevEditedRule = '';
var cssDB;
var styleOutput;
var stylesToCheck;

var measureType = 'px';
var gridValue = '';
var labelBtn;
